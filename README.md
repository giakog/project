# REAL TIME EMBEDDED SYSTEM PROJECT:  CANNON GAME #

Simulate​ ​ a ​ ​ cannon​ ​ controlled​ ​ by​ ​ the​ ​ user​ ​ that​ ​ must​ ​ shoot​ ​ a ​ ​ ball​ ​ to​ ​ catch​ ​ a
target​ ​ that​ ​ moves​ ​ slowly​ ​ on​ ​ the​ ​ ground​ ​ on​ ​ the​ ​ other​ ​ side​ ​ of​ ​ a ​ ​ wall.​ ​ Position​ ​ and
height​ ​ of​ ​ the​ ​ wall​ ​ are​ ​ randomly​ ​ generated​ ​ after​ ​ each​ ​ shot.​ ​ Use​ ​ 3D​ ​ graphics​ ​ if​ ​ you
want.

## How to launch the game ##
1. Open a terminal inside the game directory
2. Execute the command: make main
3. To launch the game execute the command: sudo ./main

### How to play the game ###
1. Select start new game (using UP or DOWN key to navigate the menu and ENTER to confirm) 
2. Select the level of difficulty (as in point 1)
3. Use the keyboard to set the power of the cannon shot (cannon ball speed); then press ENTER
4. Use the UP or DOWN arrow to increase or decrease the firing angle
5. Press ENTER to fire the cannon or BACKSPACE to modify cannon shot power
6. Then after the end of the round press R to retry, BACKSPACE to change difficulty or ESC to exit game
