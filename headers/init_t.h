
#if !defined INIT_T_H
#define INIT_T_H

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <allegro.h>
#include "ptask.h"



#define ALLEGRO_NO_FIX_ALIASES  // fix for the new version of glibc

#define XWIN        800         // Window width         
#define YWIN        600         // Window height

#define N_exp       29          // Bitmaps conteined in the explosion spritesheet
#define N_tg        16          // Bitmaps conteined in the motion spritesheet

#define XMIN        0			
#define XMAX        770


#define PER         20          // Task period in ms
#define DREL        20          // Relative deadline in ms
#define PRIO        80          // Task priority

#define FLOOR       61			// Position of the ground
#define MAX_BALLS   1
#define PI          3.141592654 // PI Greek

#define TILE        20			// Dimension of 1 bitmap block for the wall
								// (20x20)
#define MAX_H       300			// Maximum wall height
#define MIN_H       100			// Minumum wall height
#define MAX_W       500			// Maximum wall orizzontal position (from left)
#define MIN_W       300			// MInumum wall orizzontal position (from left)
#define MAX_S       3			// Maximum moving target speed
#define MIN_S       1			// Minimum moving target speed
#define MAX_P       750			// Maximum moving target starting position

//PTHREAD MUTEX AND CONDITION VARIABLES (cond_var)
extern pthread_mutex_t mxa;            // PTHREAD_MUTEX_INITIALIZER;
extern pthread_cond_t mxb;             // cond_var used to sync ball and drawer
extern pthread_cond_t mxt;             // conda_var used to sync target and drawer             
extern int b_mxb, c_mxb, b_mxt, c_mxt, b_mxd, c_mxd;       



//  ############################################################################
//  Global variables
//  ############################################################################


extern BITMAP *logo;					// Used to memorize the "cannon game" logo
extern BITMAP *bg;						// Used to memorize the game background
extern BITMAP *start;					// Used to memorize the "start new game" sprite
extern BITMAP *exit_game;				// Used to memorize the "End game" sprite
extern BITMAP *c_ball;					// Used to memorize the cannon ball sprite
extern BITMAP *effect[N_exp];			// Used to memorize the explosion spritesheet
extern BITMAP *target[N_tg];			// Used to memorize the 
										// target animation spritesheet
extern BITMAP *wall_texture;			// Used to memorize 1 tile of wall's sprite
extern BITMAP *cannon;					// Used to memorize the cannon sprite
extern BITMAP *base;                    // USed to memorize the cannon base sprite
extern BITMAP *hit;						// Used to memorize the hit message sprite
extern BITMAP *miss;					// Used to memorize the miss message sprite
extern BITMAP *sel;                    // Used to memorize the select_diff sprite
extern BITMAP *easy;                   // Used to memorize the easy diff sprite
extern BITMAP *medium;                 // Used to memorize the medium diff sprite
extern BITMAP *hard;                   // Used to memorize the hard diff sprite


extern int last_proc;				// Last created thread							
extern int max_proc; 				// Max concurrent thread
extern int h;						// Temporary memorize Wall height
extern float U;						// Ball speed
extern float angle;					// Cannon angle
extern float g;						// Gravity acceleration 
extern int ntask;					// Number of active task
extern int ntask_b;					// Number of active ball
extern int win;						// Win condition (0 = target miss; 
									// 1 = target hit)
extern int dif;                   	// Difficulty of the game (0 easy, 1 medium, 2 hard)
extern char an[20];
extern int state;					// Game state: -1 main menu, 0 speed and angle
									// selection, 1 ball in motion, 3 game ended
extern char vel_txt[255];
extern char str[3];					// Used to temporary save the ball speed inputed
									// by the user
extern int score;                   // User score
extern int attempt;                 // Number of user attempt

//  ############################################################################
//  Wall, Target and Ball structs
//  ############################################################################


struct wall_struct {
    int hight;
    int pos;
} wall;

struct target_struct {
    int pos_x;
    int pos_y;
    int speed;
} tg;

struct ball_struct {
    int pos_x;
    int pos_y;
    int impact;
	float U;
} ball;



//  ############################################################################
//  Declaration of functions
//  ############################################################################


extern void animation_explosion();
extern void animation_movment();

extern void main_menu();
extern void diff_menu();

extern void options();

extern void init();

extern void set_target();

extern int set_wall();
extern void draw_wall(int);

extern void set_vel(int, int, int);

extern void set_score();

extern int check_impact_wall();
extern int check_impact_target();

void end_ball();
void shot_ball();
extern void create_ball();

void moving_target();
extern void create_target();

void pre_game(int);
void show_updated_ball(int);
void show_explosion(int, int);
void show_lost();
void show_win();
void drawer();
extern void create_drawer();



#endif // INIT_T_H
