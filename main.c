#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <allegro.h>
#include "ptask.h"
#include "init_t.h"



int main(int argc, char **argv) {
	// started set to 1 after a difficulty is selected; 
	// status is set to 1 when "star new game" is selected, 
	// 0 when exit is selected
    int started, status, k, c;
    struct wall;
    struct tg;
    srand(time(0));
    started = 0;
    status = 1;
    if(geteuid() != 0) {    //check if user is root
        printf("PTASK can only run with super user privileges.\n");
        printf("Please start the app using sudo\n");
        exit(EXIT_SUCCESS);
    }

    snprintf(vel_txt, 255, "Shot Power min %d, max %d:", 50, 150);
    init();
    snprintf(an, 20, "Angle: %d", (int)angle);
    main_menu();
    circlefill(screen, XWIN / 2 - 106 - 20, 260, 6, 0);
    /* CREATE DRAWING TASK */
    create_drawer();
    do {     
        k = 0;
        if (keypressed()) {
            c = readkey();
            k = c >> 8;
        }
        if (k == KEY_DOWN && started == 0 && dif == -1) {
            main_menu();
            if (status == 1) {
                status = 0;
                circlefill(screen, XWIN/2 - 68 - 20, 300, 6, 0);
            }
            else if (status == 0) {
                status = 1;
                circlefill(screen, XWIN/2 - 106 - 20, 260, 6, 0);
            }
        }
        if (k == KEY_UP && started == 0 && dif == -1) {
            main_menu();
            if (status == 1) {
                status = 0;
                circlefill(screen, XWIN/2 - 68 - 20, 300, 6, 0);
            }
            else if (status == 0) {
                status = 1;
                circlefill(screen, XWIN/2 - 106 - 20, 260, 6, 0);
            }
        }

        if (k == KEY_UP && started == 1 && ntask_b < MAX_BALLS && win == -1) {
            if (angle < 90) {
                angle += 10;
                snprintf(an, 20, "Angle: %d", (int)angle);
            }
        }
        if (k == KEY_DOWN && started == 1 && ntask_b < MAX_BALLS && win == -1) {
            if (angle > 0) {
                angle -= 10;
                snprintf(an, 20, "Angle: %d", (int)angle);
            }
        }

        if (k == KEY_DOWN && started == 0 && status == 1 && dif != -1) {
            diff_menu();
            switch (dif) {
                case 0:
                dif = 1;
                circlefill(screen, XWIN/2 - 70, 325, 4, 0);
                break;
                
                case 1:
                dif = 2;
                circlefill(screen, XWIN/2 - 70, 365, 4, 0);  
                break;

                case 2:
                dif = 0;
                circlefill(screen, XWIN/2 - 70, 285, 4, 0);
                break; 
            }
        }
        if (k == KEY_UP && started == 0 && status == 1 && dif != -1) {
            diff_menu();
            switch (dif) {
                case 0:
                dif = 2;
                circlefill(screen, XWIN/2 - 70, 365, 4, 0);
                break;
                
                case 1:
                dif = 0;
                circlefill(screen, XWIN/2 - 70, 285, 4, 0);  
                break;

                case 2:
                dif = 1;
                circlefill(screen, XWIN/2 - 70, 325, 4, 0);
                break; 
            }
        }
        if (k == KEY_ENTER && started == 0 && status == 0) break;

        if (k == KEY_ENTER && started == 0 && status == 1 && dif == -1) {
            dif = 0;
            diff_menu();
            circlefill(screen, XWIN/2 - 70, 285, 4, 0);
        }        
    
        else if (k == KEY_ENTER && started == 0 && status == 1 && dif != -1) {
            options();
            set_score();
            printf("Starting game at difficulty: %d\n", dif);
            draw_sprite(screen, bg, 0, 0);
            status = -1;
            state = 0;
            h = set_wall();
            draw_wall(h);
            set_target();
            printf("Setting target position: %d\n", tg.pos_x);
            /* RE-CREATE MOOVING TARGET TASK */
            create_target();
            set_vel(100, 50, 150);
            printf("setting velocity: %d\n", (int)ball.U);
            started = 1;
        }
        else if (k == KEY_ENTER && started == 1 && ntask_b < MAX_BALLS && win == -1) {
            pthread_mutex_lock(&mxa);
            state = 1;
            ++attempt;
            pthread_mutex_unlock(&mxa);

            /* CREATE MOOVING BALL TASK */
            create_ball();

        }
        if (k == KEY_R && started == 1 && state==3 && ntask_b < MAX_BALLS) {
            printf("Starting game at difficulty: %d\n", dif);
            state = 0;
            win = -1;
            ball.impact = -1;
            bg = load_bitmap("./data/background.bmp", NULL);
            h = set_wall();
            draw_wall(h);
            draw_sprite(screen, bg, 0,0);
            options();
            set_score();
            set_target();
            printf("Re-setting target position: %d\n",tg.pos_x);
            /* CREATE MOOVING TARGET TASK */
            create_target();
            printf("setting_vel\n");
            set_vel(100, 50, 150);
            printf("setting velocity: %d\n", (int)ball.U);
            
        }
        if (k == KEY_BACKSPACE && started == 1 && ntask == 1 && (state == 0 || state ==3)) {
            dif = 0;
            win = -1;
            started = 0;
            status = 1;
            bg = load_bitmap("./data/background.bmp", NULL);
            diff_menu();
            circlefill(screen, XWIN/2 - 70, 285, 4, 0);
        }
        else if (k ==KEY_BACKSPACE  && started == 1 && ntask_b < MAX_BALLS && state == 0) {
            printf("setting_vel\n");
            set_vel(100, 50, 150);
            printf("setting velocity: %d\n", (int)ball.U);
        }
    }while(k!=KEY_ESC);

    allegro_exit();
    return 0;
}

