#include "init_t.h"


//  ############################################################################
//  set_vel function - Used to read and save the ball velocity from user input 
//  ############################################################################


void set_vel(int off, int min, int max) {
    // Type specifies if input is a number, letters only string,
    // or all char string
    int     c, k, f, i, t;
    char    pos;
    float ret;

    t = 0;
    f = 0;
    pos = 0;

    for (i=0; i<3; i++) {
        str[i] = 0;
    } 

    while (1) {
        k = 0;
        if (keypressed()) {
            k = readkey();
            // add character if string is not full
            if (pos < 3) {
                if((k & 0xff) >= '0' && (k & 0xff) <= '9') {
                    str[pos] = k & 0xff;
                    pos++;
                }
            }
            //  remove character if string is not empty
            if (pos > 0 && (k >> 8) == KEY_BACKSPACE) {
                str[pos-1] = 0;
                pos--;
            } 
            else k = k >> 8; 
        }
        if (k == KEY_ENTER && atoi(str) >= min && atoi(str) <= max) {
            ball.U = atoi(str);
            return;
        }
    }
}



//  ############################################################################
//  Check_impact functions - used to check the ball's impact with wall or target 
//  ############################################################################


int check_impact_wall() {
    int ret;

    if ((ball.pos_x >= wall.pos - 20 && ball.pos_x <= wall.pos + 20)
    	&& ball.pos_y <= wall.hight +20) ret = 1;
    else ret = 0;

    return ret;
}

int check_impact_target(){
    int ret;

    if ((ball.pos_x >= tg.pos_x - 20 && ball.pos_x <= tg.pos_x + 20)
    	&& ball.pos_y <= tg.pos_y) ret = 1;
    else ret = 0;
    
    return ret;
}



//  ############################################################################
//  Ball Thread - Used to change the ball's position throught time
//  It's ended if the ball touch the wall or the ground 
//  ############################################################################

void end_ball() {
    --ntask;
    --ntask_b;
    --c_mxb;
    printf("Ball thread ended\n");
}

void shoot_ball() {
    int ret, i, x0;
    float dx, dy, dt, t, tx;

    i = ptask_get_index();
    x0 = XMIN + 40;
    dx = ball.U * cos(angle * PI / 180);    
    dy = ball.U * sin(angle * PI / 180);
    t = tx = 0.0;
    ball.impact = 0;
    ball.pos_y = FLOOR + 15;
    dt = ptask_get_period(i, MILLI) / 100.;
    pthread_mutex_lock(&mxa);
    ++c_mxb;
    pthread_mutex_unlock (&mxa);

    while (1) {
        if (ball.pos_y >= FLOOR) {
            ball.pos_x = x0 + dx * tx;
            ball.pos_y = FLOOR + 15 + dy * t - .5 * g * t * t;
            ret = check_impact_wall();
            if (ret == 1) {
                printf("Wall impact\n");
                ball.impact = 1;
                win = 0;
                end_ball();
                return;
            }
        }
        else {
            ball.impact = 1;
            win = 0;
            end_ball();
            return;
        }
        dy = .99 * dy;
        t += dt;
        tx += dt;
        pthread_mutex_lock(&mxa);
        ++b_mxb;
        pthread_cond_wait(&mxb, &mxa);
        pthread_mutex_unlock(&mxa);
        if (ball.impact != 0) {
            end_ball();
            return;
        }
        ptask_wait_for_period();
    }
}

void create_ball(){
    int i;
    tpars params;

    i = 0;
    params = TASK_SPEC_DFL;
    params.period = tspec_from(PER, MILLI);
    params.rdline = tspec_from(DREL, MILLI);
    params.priority = PRIO - i;
    params.measure_flag = 1;
    params.act_flag = NOW;
    /* a round robin assignment */
    params.processor = last_proc++;
    if (last_proc >= max_proc) last_proc = 0;
    i = ptask_create_param(shoot_ball, &params);
    if (i != -1) {
        printf("Task %d created and activated\n", i);
        ++ntask;
        ++ntask_b;
    }
    else {
        allegro_exit();
        printf("Error creating ball task!\n");
        exit(-1);
    }    
}



//  ############################################################################
//  Moving Target Thread - Used to change the target position throught time
//  ############################################################################


void moving_target() {
    int i, dir, y;
    float dt;
    
    i = ptask_get_index();
    dir = 0;
    y = YWIN - FLOOR - 20;
    dt = ptask_get_period(i, MILLI) / 100.;
    pthread_mutex_lock(&mxa);
    ++c_mxt;
    pthread_mutex_unlock(&mxa);
    
    while (win == -1) {
        if (dir == 0) {
            tg.pos_x = tg.pos_x - tg.speed;
            if (tg.pos_x < wall.pos + 20) dir = 1;
        }
        if (dir == 1) {
            tg.pos_x = tg.pos_x + tg.speed;
            if (tg.pos_x > XWIN - 20) dir = 0;
        }
        pthread_mutex_lock(&mxa);
        ++ b_mxt;
        pthread_cond_wait(&mxt, &mxa);
        pthread_mutex_unlock (&mxa);

        ptask_wait_for_period();

    }
    --ntask;
    printf("Target thread ended\n");
}

void create_target() {
    int i;
    tpars params;
    
    i = 0;
    params = TASK_SPEC_DFL;
    params.period = tspec_from(PER, MILLI);
    params.rdline = tspec_from(DREL, MILLI);
    params.priority = PRIO - i;
    params.measure_flag = 1;
    params.act_flag = NOW;
    params.processor = last_proc++;
    if (last_proc >= max_proc) last_proc = 0;
    i = ptask_create_param(moving_target, &params);
    if (i!= -1) {
        printf("Task %d created and activated\n", i);
        ++ntask;
    }
    else {
        allegro_exit();
        printf("Error creating taget task!\n");
        exit(-1);
    }
}



//  ############################################################################
//  Drawer Thread - Used to draw everything during the game phase.
//  It also call the check_impact_target fucntion and  shows the win or lose game 
//  message accordly.
//  ############################################################################


void pre_game(int n) {
    draw_sprite(screen, bg, 0,0);
    pthread_mutex_lock(&mxa);
    rotate_sprite(screen, cannon, XMIN + 15, YWIN - (FLOOR + 55), 
        -ftofix(angle));
    draw_sprite(screen, base, XMIN, YWIN - (FLOOR + 20));
    textout_ex(screen, font, an, XMIN + 80, YWIN - 140, 0, -1);
    textout_ex(screen, font, vel_txt, XMIN + 8, YWIN - 150, 0, -1);
    textout_ex(screen, font, str, XMIN + text_length(font,vel_txt) + 20, 
        YWIN - 150, 0, -1);

    pthread_mutex_unlock (&mxa);
    pthread_mutex_lock(&mxa);
    if (b_mxt != 0 && c_mxb == 0) {
        draw_sprite(screen,target[n], tg.pos_x, YWIN - tg.pos_y);
        --b_mxt;
        pthread_cond_signal(&mxt);
    }
    pthread_mutex_unlock(&mxa);
    animation_movment();
}

void show_updated_ball(int j) {
    draw_sprite(screen, bg, 0,0);
    draw_sprite(screen, c_ball, ball.pos_x, YWIN - ball.pos_y);
    rotate_sprite(screen, cannon, XMIN + 15, YWIN - (FLOOR + 55), 
        -ftofix(angle));
    draw_sprite(screen, base, XMIN, YWIN - (FLOOR + 20));
    draw_sprite(screen,target[j], tg.pos_x, YWIN - tg.pos_y);
    --b_mxb;
    --b_mxt;
    pthread_cond_signal(&mxt);
    pthread_cond_signal (&mxb);
    pthread_mutex_unlock(&mxa);
    animation_movment();
}

void show_explosion(int k, int j) {
    int pos_x;
    int pos_y;

    pos_x = ball.pos_x - 10;
    pos_y = YWIN - ball.pos_y - 130; 
    draw_sprite(screen, bg, 0, 0);
    draw_sprite(screen, effect[k], pos_x, pos_y);
    if (win == 0)
        draw_sprite(screen,target[j], tg.pos_x, YWIN - tg.pos_y);
    rotate_sprite(screen, cannon, XMIN + 15, YWIN - (FLOOR + 55), -ftofix(angle));
    draw_sprite(screen, base, XMIN, YWIN - (FLOOR + 20));
    if (b_mxt != 0) pthread_cond_signal (&mxt);
    if (b_mxb != 0) pthread_cond_signal (&mxb);
    pthread_mutex_unlock(&mxa);
    animation_explosion();
}

void show_lost() {
    draw_sprite(bg, miss, XWIN / 2 - 165 , 150);
    textout_centre_ex(bg, font, 
        "Press R to retry, BACKSPACE to change difficulty or ESC to exit game", 
        XWIN/2, 200, 0, -1);
    draw_sprite(screen, bg, 0, 0);
    draw_sprite(screen, target[0],tg.pos_x, YWIN - tg.pos_y);
    rotate_sprite(screen, cannon, XMIN + 15, YWIN - (FLOOR + 55), -ftofix(angle));
    draw_sprite(screen, base, XMIN, YWIN - (FLOOR + 20));
    state = 3;
}

void show_win() {
    score = score + 1 + dif;
    draw_sprite(bg, hit, XWIN / 2 - 130, 150);
    textout_centre_ex(bg, font, 
        "GOOD JOB!! Press R to retry,BACKSPACE to change difficulty or ESC to exit game", 
        XWIN/2, 200, 0, -1);
    draw_sprite(screen, bg, 0, 0);
    rotate_sprite(screen, cannon, XMIN + 15, YWIN - (FLOOR + 55), -ftofix(angle));
    draw_sprite(screen, base, XMIN, YWIN - (FLOOR + 20));
    state = 3;
}

void drawer() {
    
    // updt used to update moving sprite only after 3 iteration
    int updt, i, n, j, k, cnd, ret;
    updt = 0;   
    i = ptask_get_index();
    n = j = k = 0;
    pthread_mutex_lock(&mxa);
    ++c_mxd;
    pthread_mutex_unlock(&mxa);

    while(1) {
        if (n ==  N_tg) n = 0;
        if(state == 0) {
            if (ntask == 2) {
                ++updt;
                cnd = k = 0;
                pre_game(n);
                if (updt == 4){
                    ++n;
                    updt = 0;
                }
            }
        }
        else if (state == 1) {
            if (j == N_tg)  j = 0;
            pthread_mutex_lock( &mxa);
            if (b_mxt != 0 && b_mxb != 0 || ntask != 3){
                ret = check_impact_target();
                if (ret == 1 && ball.impact == 0) {
                    printf("TARGET HIT\n");
                    ball.impact = win = 1;
                }
                if (ball.impact == 0) {
                    ++updt;
                    show_updated_ball(j);
                    if(updt == 4) {
                        ++j;
                        updt = 0;
                    }
                }
                else {
                    if (k == N_exp){
                        k = 0;
                        cnd = 1;
                    }
                    if (cnd == 0) {
                        show_explosion(k, j);
                        ++k;
                    }
                    else pthread_mutex_unlock (&mxa);
                    if (win == 0 && cnd == 1) show_lost();
                    else if (win == 1 && cnd == 1) show_win();
                }   
            }
            else pthread_mutex_unlock (&mxa);
        }
        ptask_wait_for_period();
    }
}

void create_drawer() {
    int i;
    tpars params;

    i = 0;
    params = TASK_SPEC_DFL;
    params.period = tspec_from(PER, MILLI);
    params.rdline = tspec_from(DREL, MILLI);
    params.priority = PRIO - i;
    params.measure_flag = 1;
    params.act_flag = NOW;
    params.processor = last_proc++;
    if (last_proc >= max_proc) last_proc = 0;

    i = ptask_create_param(drawer, &params);
    if (i != -1) {
        printf("Task %d created and activated\n", i);
        ++ntask;
    }
    else {
        allegro_exit();
        printf("Error creating drawer task!\n");
        exit(-1);
    }
}
