#include "init_t.h"

pthread_mutex_t mxa;            // PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t mxb;             // cond_var used to sync ball and drawer
pthread_cond_t mxt;             // conda_var used to sync target and drawer             
int b_mxb, c_mxb = 0;            
int b_mxt, c_mxt = 0;
int b_mxd, c_mxd = 0;       



//  ############################################################################
//  Global variables
//  ############################################################################


BITMAP *logo;					// Used to memorize the "cannon game" logo
BITMAP *bg;						// Used to memorize the game background
BITMAP *start;					// Used to memorize the "start new game" sprite
BITMAP *exit_game;				// Used to memorize the "End game" sprite
BITMAP *c_ball;					// Used to memorize the cannon ball sprite
BITMAP *effect[N_exp];			// Used to memorize the explosion spritesheet
BITMAP *target[N_tg];			// Used to memorize the 
								// target animation spritesheet
BITMAP *wall_texture;			// Used to memorize 1 tile of wall's sprite
BITMAP *cannon;					// Used to memorize the cannon sprite
BITMAP *base;                   // Used to memorize the cannon base sprite
BITMAP *hit;					// Used to memorize the hit message sprite
BITMAP *miss;					// Used to memorize the miss message sprite
BITMAP *sel;                    // Used to memorize the select_diff sprite
BITMAP *easy;                   // Used to memorize the easy diff sprite
BITMAP *medium;                 // Used to memorize the medium diff sprite
BITMAP *hard;                   // Used to memorize the hard diff sprite


int last_proc = 0;				// Last created thread							
int max_proc = 3; 				// Max concurrent thread
int h;							// Temporary memorize Wall height
float U;						// Ball speed
float angle;					// Cannon angle
float g = 9.8;					// Gravity acceleration 
int ntask = 0;					// Number of active task
int ntask_b = 0;				// Number of active ball
int win = -1;					// Win condition (0 = target miss; 
								// 1 = target hit)
int dif = -1;                   // Difficulty of the game (0 easy, 1 medium, 2 hard)
char an[20];
int state = -1;					// Game state: -1 main menu, 0 speed and angle
								// selection, 1 ball in motion, 3 game ended
char vel_txt[255];
char str[3];					// Used to temporary save the ball speed inputed
								// by the user                                
int score = 0;                  // User score
int attempt = 0;                // Number of user attempt


//  ############################################################################
//  Animation Functions, graphical function for menu and options tips function
//  ############################################################################


// Defines the waiting time between each explosion frame
void animation_explosion() {
    int i;

    for (i=0; i!=9000000; ++i);
}
// Defines the waiting time between each target movement frame
void animation_movment() {
    int i;

    for (i=0; i!=9900000; ++i);
}
// Used to rapidly visualize the Main Menu
void main_menu() {

    draw_sprite(screen, bg, 0, 0);
    draw_sprite(screen, logo, XWIN/2 - 84, 180);
    draw_sprite(screen, start,XWIN/2 - 106, 250);
    draw_sprite(screen, exit_game,XWIN/2 - 68, 290);

}
void diff_menu() {
    draw_sprite(screen, bg, 0, 0);
    draw_sprite(screen, sel, XWIN/2 - 110, 240);
    draw_sprite(screen, easy, XWIN/2 - 60, 280);
    draw_sprite(screen, medium, XWIN/2 - 60, 320);
    draw_sprite(screen, hard, XWIN/2 - 60, 360);
    textout_ex(screen, font, "(Fixed target speed and wall data)", XWIN/2 - 5, 288, 0, -1);
    textout_ex(screen, font, "(Fixed target speed; random wall data)", XWIN/2 + 15, 328, 0, -1);
    textout_ex(screen, font, "(All random)", XWIN/2 - 5, 368, 0, -1);
}

//Shows the games function (how to play the game) for the first game run
void options() {
    textout_ex(bg, font, "- First set ball vel then press ENTER to confirm" ,
    	20, 50, 0, -1);
    textout_ex(bg, font, "- Then use the UP and DOWN arrow to change the angle", 
    	20, 65, 0, -1);
    textout_ex(bg, font, "- You can  press BACKSPACE to return to the previous step",
        20, 80, 0, -1);
    textout_ex(bg, font, "- then press ENTER to shoot the ball", 
    	20, 95, 0, -1);
}



//  ############################################################################
//  INIT: allegro configuration, colors, bitmap initialization 
//  and default cannon angle
//  ############################################################################


void init() {
    int i;
    char buf[30];
    char buf_1[30];
    char buf_2[30];

    allegro_init();
    set_color_depth(32);
    set_gfx_mode(GFX_AUTODETECT_WINDOWED, XWIN, YWIN, 0, 0);
    install_keyboard();
    srand(time(NULL));

    bg = load_bitmap("./data/background.bmp", NULL);
    logo = load_bitmap("./data/logo.bmp", NULL);
    start = load_bitmap("./data/new_game.bmp", NULL);
    exit_game = load_bitmap("./data/exit_game.bmp", NULL);
    c_ball = load_bitmap("./data/ball.bmp", NULL);
    wall_texture = load_bitmap("./data/wall.bmp", NULL);
    cannon = load_bitmap("./data/cannon.bmp", NULL);
    base = load_bitmap("./data/base.bmp", NULL);
    hit = load_bitmap("./data/hit.bmp", NULL);
    miss = load_bitmap("./data/miss.bmp", NULL);
    sel = load_bitmap("./data/select.bmp", NULL);
    easy = load_bitmap("./data/easy.bmp", NULL);
    medium = load_bitmap("./data/medium.bmp", NULL);
    hard = load_bitmap("./data/hard.bmp", NULL);

    for (i=0; i<N_exp; i++) {
        snprintf(buf_1, 30, "./data/explosion/exp%d.bmp", i + 1);
        effect[i] = load_bitmap(buf_1, NULL);
    }
    for (i=0; i<N_tg; i++) {
        snprintf(buf_2, 30, "./data/target/target%d.bmp", i + 1);
        target[i] = load_bitmap(buf_2, NULL);
    }

    angle = 30;
    ptask_init(SCHED_FIFO, GLOBAL,PRIO_INHERITANCE);
}



//  ############################################################################
//  Randoms game component initialization functions - Used to define target and
//  wall
//  ############################################################################


//Define the random initial position and velocity of the target
void set_target() {
    if (dif == 2)
        tg.speed = (rand () %(MAX_S - MIN_S + 1)) + MAX_S;
    else 
        tg.speed = 1;
    tg.pos_x = (rand() % (MAX_P - wall.pos + 1)) + wall.pos;
    tg.pos_y = FLOOR + 20;
}

//Define the random initial height and position of the wall
int set_wall() {
    if (dif == 0){
        h = 150;
        wall.pos = 250;
    }
    else {
        h = (rand() % (MAX_H - MIN_H + 1)) + MIN_H;
        wall.pos = (rand() % (MAX_W - MIN_W + 1)) + MIN_W;
    }
    return h;
}

//Used to draw the stacked sprites of the wall on the background
void draw_wall(int h) {
    wall.hight = FLOOR;
    for (wall.hight=FLOOR; wall.hight<=h; wall.hight+=20) {
        draw_sprite(bg, wall_texture, wall.pos, YWIN - wall.hight);
    }

}

void set_score() {
    char msg[30];
    snprintf(msg, 30, "Try: %d SCORE: %d", attempt, score);
    textout_ex(bg, font, msg, 650, 50, 0, -1);
}


